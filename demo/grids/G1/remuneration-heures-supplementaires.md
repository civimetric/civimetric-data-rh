# Rémunération des heures supplémentaires

**Aujourd'hui**, les heures supplémentaires sont aujourd'hui payées _115%_ dans l'entreprise B et _105%_ dans l'entreprise A. 

**Par comparaison**, les heures supplémentaires dans le même domaine d'activité sont payées _107%_ en moyenne.

![ALT Pièces](remuneration-heures-supplementaires.jpg "Rémunération")