# Congés payés

**Aujourd’hui**, les congés payés représentent _25 jours_ ouvrables pour l'entreprise A et _20 jours_ pour l'entreprise B.

**Par comparaison**, la moyenne dans le domaine agro-alimentaire est de _22 jours_ ouvrables.

![Plage](conges-payes.jpg)