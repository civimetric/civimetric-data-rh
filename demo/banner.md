<div class="bg-info d-flex flex-row mb-4 text-white">
  <img alt="Logo de Civimetric" class="align-self-center mb-1 ml-1 mr-1 mt-1" src="../logo-blanc.png" style="height: 64px; width: auto;">
  <div class="align-self-center flex-grow-1 text-center">
    <h1>Consultation des collaborateurs</h1>
    <h2>Donnez votre avis sur l'harmonisation des bénéfices employés</h2>
  </div>
</div>
