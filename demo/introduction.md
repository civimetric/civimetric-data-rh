Dans le cadre de la fusion de l'entreprise A et de l'entreprise B, il est nécessaire d'harmoniser les bénéfices de l'ensemble des collaborateurs.
Cette harmonisation a pour objectif d'être au plus proche des souhaits des collaborateurs tout en respectant la réalité des contraintes notamment économiques.

Nous vous proposons d'**imaginer que vous êtes au sein du comité de direction de l'entreprise**.
Quel serait à votre avis la meilleure combinaison de bénéfices pour l'ensemble des collaborateurs ?

Ce questionnaire devrait vous prendre **10 minutes** au total.
Bonne séance !
